#------------------------------------------------------------------------------
# Most of the things that I have here for hexagonal grids are taken, the concept
# not the actual code, from:
#   http://www.redblobgames.com/grids/hexagons/#rings (Checked on 2013-08-13)
#
# And most of it is done for pointy topped hexagons
module HexagonalGrid

# ------------------------------------------------------------------------------
# Uniformly place users in an hexagonal cell
function place_users(radius, bs_position::Array{Float64,1}, number_of_users::Int64, min_r = 0, max_r = 1)
  if min_r < 0 || min_r > 1 || max_r > 1 || max_r < 0 || min_r > max_r
    error("Invalid limits for the placement of a point in the cell")
  end

  M = [[cos(pi/2) -sin(pi/2)], [sin(pi/2) cos(pi/2)]] * [[-cos(pi/6) cos(pi/6) 0.0], [-sin(pi/6) -sin(pi/6) 1.0]] * radius / sqrt(3)

  positions = Array(Array{Float64,1}, number_of_users)
  for ii in 1:number_of_users
    uvw = get_position_in_hexagon(min_r, max_r)
    positions[ii] = M * uvw + bs_position
  end

  return positions
end

# ------------------------------------------------------------------------------
# Generate users in several cells
function place_users(radius, bs_positions::Array{Array{Float64,1},1}, users_per_cell::Int64 = 1, min_r = 0, max_r = 1)
  if min_r < 0 || min_r > 1 || max_r > 1 || max_r < 0 || min_r > max_r
    error("Invalid limits for the placement of a point in the cell")
  end
  M = [[cos(pi/2) -sin(pi/2)], [sin(pi/2) cos(pi/2)]] * [[-cos(pi/6) cos(pi/6) 0.0], [-sin(pi/6) -sin(pi/6) 1.0]] * radius / sqrt(3)

  number_of_bs = size(bs_positions, 1)
  number_of_ms = number_of_bs * users_per_cell

  positions = Array(Array{Float64,1}, number_of_ms)
  idx = 1
  for ii in 1:number_of_bs
    temp_positions = place_users(radius, bs_positions[ii], users_per_cell, min_r, max_r)
    for jj in 1:users_per_cell
      positions[idx] = temp_positions[jj]
      idx += 1
    end
  end

  return positions
end

# ------------------------------------------------------------------------------
# Place users in a cluster, so that the users in the external cells are located
# closer to the cluster edge
function place_users_cluster(radius, bs_positions::Array{Array{Float64,1},1}, users_per_cell, factor, shift = false)

  number_of_bs = size(bs_positions, 1)
  number_of_ms = number_of_bs * users_per_cell
  if shift # triangular
    tiers = sqrt(number_of_bs/3) - 1
    last_interior_idx = 3*(tiers)^2
  else # hexagonal
    tiers = (-1 + sqrt(1 + (4./3.) * (number_of_bs - 1))) / 2
    if tiers == 0
      last_interior_idx = 0
    else
      last_interior_idx = 3 * (tiers - 1) * tiers + 1
    end
  end

  positions = Array(Array{Float64,1}, number_of_ms)
  idx = 1
  for ii in 1:last_interior_idx
    temp_positions = place_users(radius, bs_positions[ii], users_per_cell, 0, 1)
    for jj in 1:users_per_cell
      positions[idx] = temp_positions[jj]
      idx += 1
    end
  end

  if shift
    size_of_last_tier = (3*(2 * (tiers) + 1))
  else
    if tiers == 0
      size_of_last_tier = 1
    else
      size_of_last_tier = 6 * tiers
    end
  end

  for ii in 1:size_of_last_tier
    directions = get_directions(tiers, ii, shift)
    temp_positions = place_users_border(radius, bs_positions[last_interior_idx + ii], users_per_cell, factor, directions)
    for jj in 1:users_per_cell
      positions[idx] = temp_positions[jj]
      idx += 1
    end
  end

  return positions
end

# ------------------------------------------------------------------------------
# get the directions that are exterior to a given cell in a given tier
function get_directions(tier, idx, shift = false)
  directions = falses(6)
  if tier == 0
    if shift
      if idx == 1
        directions[[3,4,5,6]] = true
      elseif idx == 2
        directions[[1,2,5,6]] = true
      elseif idx == 3
        directions[[1,2,3,4]] = true
      else
        error("Wrong index for tier $(tier)")
      end
    else
      directions = trues(6)
    end
  else
    temp_idx = 1
    for dir in 1:6
      number_of_steps = tier
      if shift
        number_of_steps = tier + 1
        if mod(dir, 2) == 0
          number_of_steps -= 1
        end
      end
      corner = true
      for ii = 1:number_of_steps
        if temp_idx == idx
          dirs = [1:6]
          dir_idx = dirs[mod(dirs - 1 + (dir - 1), 6) + 1]
          if corner # for corners, three sides are edge sides
            directions[dir_idx[end-2:end]] = true 
          else # for non corners, only two sides are edge sides
            directions[dir_idx[end-1:end]] = true
          end
          return directions
        else
          temp_idx += 1
        end
        corner = false
      end
    end
  end
  return directions
end

# ------------------------------------------------------------------------------
# Place the users only on some of the borders of the cell. This is thought to be
# used to place users at the border of the cluster.
function place_users_border(radius, bs_position::Array{Float64,1}, number_of_users::Int64, factor = 0, directions::BitArray{1} = trues(6))
  if directions == falses(6)
    error("There should be at least one direction selected")
  end
  M = [[cos(pi/2) -sin(pi/2)], [sin(pi/2) cos(pi/2)]] * [[-cos(pi/6) cos(pi/6) 0.0], [-sin(pi/6) -sin(pi/6) 1.0]] * radius / sqrt(3)

  positions = Array(Array{Float64,1}, number_of_users)
  for ii in 1:number_of_users
    while true
      uvw = get_position_in_hexagon(0, 1)
      if check_place_directions(uvw, directions, factor)
        positions[ii] = M * uvw + bs_position
        break
      end
    end
  end
  return positions
end

function check_place_directions(uvw::Array{Float64,1}, directions::BitArray{1}, factor)
  flag = false
  if directions[1]
    flag = flag || uvw[3] < -factor
  end
  if directions[2]
    flag = flag || uvw[2] > factor
  end
  if directions[3]
    flag = flag || uvw[1] < -factor
  end
  if directions[4]
    flag = flag || uvw[3] > factor
  end
  if directions[5]
    flag = flag || uvw[2] < -factor
  end
  if directions[6]
    flag = flag || uvw[1] > factor
  end
  return flag
end

# ------------------------------------------------------------------------------
# get the cubic coordinates of a point within a normalized (radius 1) cell
# (flat topped), in the range from min_r to max_r, if min_r=0 and max_r=1 then
# the point could be anywhere in the cell.
function get_position_in_hexagon(min_r = 0, max_r = 1)
  if min_r < 0 || min_r > 1 || max_r > 1 || max_r < 0 || min_r > max_r
    error("Invalid limits for the placement of a point in the cell")
  end
  min_r_2 = min_r*min_r
  max_r_2 = max_r*max_r
  reset_to_three(x) = x==0?3:x
  uvw = [0.0, 0.0, 0.0]
  idx1 = rand(1:3)
  idx2 = map(reset_to_three, (idx1 + 1)%3)
  idx3 = map(reset_to_three, (idx1 + 2)%3)
  uvw[idx1] = sign(randn()) * sqrt(min_r_2 + rand()*(max_r_2 - min_r_2))
  uvw[idx2] = - uvw[idx1] * rand()
  uvw[idx3] = - uvw[idx1] - uvw[idx2] 
  return uvw
end

# ------------------------------------------------------------------------------
# Abstraction to generate the grid layout
function get_grid(radius, tiers, shift = false)
  if shift
    return triangular_grid(radius, tiers)
  else
    return hexagonal_grid(radius, tiers)
  end
end

# ------------------------------------------------------------------------------
# Get the positions of BS placed in an hexagonal layout
function hexagonal_grid(radius, tiers)
  M = [[-cos(pi/6) cos(pi/6) 0.0], [-sin(pi/6) -sin(pi/6) 1.0]]*radius
  number_of_cells = 3 * tiers * (tiers + 1) + 1
  positions = Array(Array{Float64,1}, number_of_cells)
  uvw = [0.0, 0.0, 0.0]
  xy = M * uvw
  positions[1] = xy
  idx = 2
  for tier in 1:tiers 
    uvw = get_ring(tier)
    for ii in 1:length(uvw)
      xy = M * uvw[ii]
      positions[idx] = xy
      idx += 1
    end
  end
  return positions
end

# ------------------------------------------------------------------------------
# Get the positions of BS placed in a triangular layout
function triangular_grid(radius, tiers)
  M = [[-cos(pi/6) cos(pi/6) 0.0], [-sin(pi/6) -sin(pi/6) 1.0]]*radius
  number_of_cells = 3 * (tiers + 1) * (tiers + 1)
  positions = Array(Array{Float64, 1}, number_of_cells)
  idx = 1 
  for tier in 0:tiers
    uvw = get_ring(tier, true)
    for ii in 1:length(uvw)
      xy = M * uvw[ii]
      positions[idx] = xy
      idx += 1
    end
  end
  return positions
end

# ------------------------------------------------------------------------------
# Get the cubic coordinates of a hexagonal ring of points placed at a given
# distance from the center
function get_ring(tier, shift = false)
  if shift
    positions = get_ring_shifted(tier)
  else
    positions = get_ring_unshifted(tier)
  end
  return positions
end

function get_ring_unshifted(tier)
  number_of_cells = 6 * tier
  positions = Array(Array{Float64,1}, number_of_cells)
  current_position = [tier, 0, -tier]
  idx = 1
  for dir in 1:6
    for ii in 1:tier
      positions[idx] = current_position
      current_position = current_position + neighbors(dir)
      idx += 1
    end
  end
  return positions
end

function get_ring_shifted(tier)
  number_of_cells = 3 * (2 * tier + 1)
  positions = Array(Array{Float64,1}, number_of_cells)
  current_position = [tier + 1, 0, -tier]
  idx = 1
  for dir in 1:6
    number_of_steps = tier + 1
    if mod(dir, 2) == 0
      number_of_steps -= 1
    end
    for ii in 1:number_of_steps
      positions[idx] = current_position
      current_position = current_position + neighbors(dir)
      idx += 1
    end
  end
  return positions
end

# ------------------------------------------------------------------------------
# Get the cubic coordinates of the movement needed to get to a neighbor in
# a given direction
function neighbors(direction)
  movements = Array(Array{Float64,1}, 6)
  movements[1] = [-1, 1, 0]
  movements[2] = [-1, 0, 1]
  movements[3] = [0, -1, 1]
  movements[4] = [1, -1, 0]
  movements[5] = [1, 0, -1]
  movements[6] = [0, 1, -1]
  return movements[direction]
end

# ------------------------------------------------------------------------------
# Write the given positions to a given file, mainly for debuggin purposes and
# to be able to plot that information with `gnuplot`
function write_positions(output_file, positions)
  fd = open(output_file, "w")
  for ii = 1:length(positions)
    @printf(fd, "%10e, %10e\n", positions[ii][1], positions[ii][2])
  end
  close(fd)
end
end
