module PrecodingMatrix

export individual_svd_precoding, zf_precoding, bd_precoding

function individual_svd_precoding(H, antennas_tx, antennas_rx)
  number_of_bs = div(size(H, 2), antennas_tx)
  U = Array(Array{Complex{Float64},2}, number_of_bs)
  S = Array(Array{Float64,1}, number_of_bs)
  W = Array(Array{Complex{Float64},2}, number_of_bs)
  for ii in 1:number_of_bs
    H_bs = H[(ii - 1) * antennas_rx + 1:ii * antennas_rx, (ii-1) * antennas_tx + 1:ii * antennas_tx]
    U_temp, S_temp, W[ii] = svd(H_bs, true)
    U[ii] = U_temp'
    S[ii] = real(S_temp)
  end
  return U, S, W
end

function zf_precoding(H)
  return pinv(H)
end

function bd_precoding(H, antennas_tx, antennas_rx)
  number_of_bs = div(size(H, 2), antennas_tx)
  number_of_ms = div(size(H, 1), antennas_rx)

  W_bd = im * zeros(number_of_bs * antennas_tx, number_of_bs * antennas_rx)
  S_bd = im * zeros(number_of_ms * antennas_rx)
  U_bd = im * zeros(number_of_ms * antennas_rx, number_of_ms * antennas_rx)

  for ms in 1:number_of_ms
    rows_to_remove = [(ms - 1) * antennas_rx + 1: ms * antennas_rx]
    H_tilde = remove_rows(H, rows_to_remove)

    (U, S, V) = svd(H_tilde, false) 
    rank_H_tilde = sum(S .> eps(1.0))
    V_tilde_0 = V[:, rank_H_tilde + 1:end]

    (U, S, V) = svd(H[rows_to_remove, :] * V_tilde_0, false)
 
    V_1 = V[:, 1:antennas_rx]

    W_bd[:, rows_to_remove] = V_tilde_0 * V_1
    S_bd[rows_to_remove] = S[1:antennas_rx]
    U_bd[rows_to_remove, rows_to_remove] = U
  end

  return U_bd, real(S_bd), W_bd
end

function remove_rows(matrix, row_idx)
  row_idx = sort(row_idx, rev = true)
  rows = 1:size(matrix, 1)
  for ii in row_idx
    new_rows = [1:ii-1, ii+1:size(matrix,1)]
    matrix = matrix[new_rows, :]
  end
  return matrix
end

end
