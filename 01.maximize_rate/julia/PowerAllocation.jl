module PowerAllocation

export equal_power_assignment, scaled_waterfilling, wf_ra

function equal_power_assignment(W, antennas_tx, antennas_rx, p_tx_max)
  number_of_bs = integer(size(W, 1) / antennas_tx)
  acc_matrix = kron(eye(number_of_bs), ones(antennas_tx)).'
  abs_W_2 = abs2(W)

  p_range::Array{Float64} = [0, 2^5]
  p_stream = p_range[2]* ones(number_of_bs * antennas_rx)
  p_tx = acc_matrix * abs_W_2 * p_stream 

  while p_tx_max - max(p_tx) > 0
    p_range[2] = 2 * p_range[2]
    p_stream = p_range[2]* ones(number_of_bs * antennas_rx)
    p_tx = acc_matrix * abs_W_2 * p_stream 
  end

  p_stream = mean(p_range) * ones(number_of_bs * antennas_rx)
  p_tx = acc_matrix * abs_W_2 * p_stream 
  flag = check_power_condition(p_tx, p_tx_max)

  while flag != 0
    if flag == 1
      p_range[2] =  mean(p_range)
    else
      p_range[1] =  mean(p_range)
    end
    p_stream = mean(p_range) * ones(number_of_bs * antennas_rx)
    p_tx = acc_matrix * abs_W_2 * p_stream 
    flag = check_power_condition(p_tx, p_tx_max)
  end

  return mean(p_range)
end

function check_power_condition(p_tx, p_tx_max)
  if sum(p_tx .> p_tx_max) == 0
    if (p_tx_max - max(p_tx)) < 1e-3
      flag = 0
    else
      flag = -1
    end
  else
    flag = 1
  end
  return flag
end

# Scaled waterfilling as described in:
#   Jun Zhang; Runhua Chen; Andrews, J.G.; Ghosh, A.; Heath, R.W., "Networked
#   MIMO with clustered linear precoding," Wireless Communications, IEEE
#   Transactions on , vol.8, no.4, pp.1910,1921, April 2009
function scaled_waterfilling(W, S, number_of_bs, antennas_tx, p_max, sigma_n)
  p_tpc = wf_ra(S, sigma_n, number_of_bs * p_max)
  max_bs = 1
  for ii in 1:number_of_bs
    size(W)
    bs_W = W[(ii - 1) * antennas_tx + 1:ii * antennas_tx, :]
    max_bs = max(max_bs, real(trace(bs_W * diagm(p_tpc) * bs_W')))
  end
  mu = p_max / max_bs
  return mu * p_tpc
end

# Waterfilling Rate Adaptive algorithm, according to Chapter 4 of Cioffi's notes
# for class EE379C
function wf_ra(channel::Array{Float64,1}, noise, p_max)
  g = channel./noise
  g_sort_idx = sortperm(g, rev = true)
  g_sort = g[g_sort_idx]
  N = length(g)
  g_sort_inv = 1 ./ g_sort
  sum_g_sort_inv = sum(g_sort_inv)
  K_tilde = p_max + sum_g_sort_inv
  N_star = N
  K = K_tilde / N_star
  while N_star > 0
    if K - g_sort_inv[N_star] <= 0
      K_tilde = K_tilde - g_sort_inv[N_star]
      N_star -= 1
      K = K_tilde / N_star
    else
      break
    end
  end
  p_tx = zeros(Float64, size(g))
  p_tx[g_sort_idx[1:N_star]] = K - g_sort_inv[1:N_star] 
  return p_tx 
end

function wf_ra(channel::Array{Array{Float64,1},1}, noise, p_max)
  number_of_bs = size(channel, 1)
  p_tx = Array(Array{Float64,1}, number_of_bs)
  for ii in 1:number_of_bs
    p_tx[ii] = wf_ra(channel[ii], noise, p_max)
  end
  return p_tx
end

# Waterfilling Margin Adaptive algorithm, according to Chapter 4 of Cioffi's
# notes for class EE379C
function wf_ma(channel::Array{Float64,1}, noise, r_min)
  g = channel./noise
  g_sort_idx = sortperm(g, rev = true)
  g_sort = g[g_sort_idx]
  g_sort_inv = 1./g_sort
  i = length(g)
  # If the rate is considered as `(1/2)*log2(1+SNR)`, then it should be a
  # `2*r_min` here, if not, it would be only `r_min`
  log2_K_tilde = r_min - sum(log2(g_sort))
  K = 2^(log2_K_tilde/i)
  while i > 0
    if K - g_sort_inv[i] <= 0
      log2_K_tilde = log2_K_tilde + log2(g_sort[i])
      i -= 1
      K = 2^(log2_K_tilde/i)
    else
      break
    end
  end
  p_tx = zeros(Float64, size(g))
  p_tx[g_sort_idx[1:i]] = K - g_sort_inv[1:i]
  return p_tx
end

end
