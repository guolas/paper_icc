module Interference

export get_interference, calculate_rate_individual

function get_interference(H, U, W, p_tx, antennas_tx, antennas_rx)
  number_of_bs = div(size(H, 2), antennas_tx)

  interf_cov = Array(Array{Complex{Float64},2}, number_of_bs)
  for ii in 1:number_of_bs
    H_ms = reshape(H[(ii - 1) * antennas_rx + 1:ii * antennas_rx, :], (antennas_rx, antennas_tx, number_of_bs))
    interf_ms = zeros(Complex{Float64}, antennas_rx, antennas_rx)
    for jj in 1:number_of_bs
      if jj != ii
        interf_ms += U[ii] * H_ms[:, :, jj] * W[jj] * diagm(p_tx[jj]) * W[jj]' * H_ms[:, :, jj]' * U[ii]'
      end
    end
    interf_cov[ii] = interf_ms
  end

  return interf_cov
end

function rate_with_interference(S, p_tx, interf, sigma_n, antennas_rx)
  number_of_bs = size(S,1)
  r = zeros(Float64, number_of_bs)
  I = eye(antennas_rx)
  for ii in 1:number_of_bs
    r[ii] = log2(real(det(I + diagm(S[ii] .* S[ii] .* p_tx[ii]) / (sigma_n .* I  .+ interf[ii]))))
  end
  return r
end
end
