module PropagationModel
include("HexagonalGrid.jl")

export get_channel_matrix

# ------------------------------------------------------------------------------
# Some sample channels with the given parameters.
# The numbers for the `channel_id` are set so that they are the same as in the
# MATLAB code that I used to use, hence the strange numbering.
function get_channel_matrix(bs_positions::Array{Array{Float64,1},1}, ms_positions::Array{Array{Float64,1},1}, antennas_tx::Int64, antennas_rx::Int64, channel_id::Int64)
  if channel_id == 4 
    warn("Assuming one user per cell")
    number_of_bs = size(bs_positions, 1)
    return flat_fading_channel_matrix(number_of_bs, antennas_tx, antennas_rx)
  elseif channel_id == 2
    return lognormal_channel_matrix(bs_positions, ms_positions, antennas_tx, antennas_rx, get_parameters(channel_id)...)
  else
    error("Channel id not supported")
  end
end

function get_channel_matrix(bs_positions::Array{Float64,2}, ms_positions::Array{Float64,2}, antennas_tx::Int64, antennas_rx::Int64, channel_id::Int64)
  new_bs_positions = Array(Array{Float64, 1}, size(bs_positions, 1))
  new_ms_positions = Array(Array{Float64, 1}, size(ms_positions, 1))

  for ii in 1:size(bs_positions, 1)
    new_bs_positions[ii] = vec(bs_positions[ii, :])
  end

  for ii in 1:size(ms_positions, 1)
    new_ms_positions[ii] = vec(ms_positions[ii, :])
  end

  return get_channel_matrix(new_bs_positions, new_ms_positions, antennas_tx, antennas_rx, channel_id)
end

# ------------------------------------------------------------------------------
# Interface to generate the channel matrix placing first the users within the
# cells, it can be specified a range, so that the users are placed only inside
# a ring identified by a `min_r` and a `max_r`
function get_channel_matrix(bs_positions::Array{Array{Float64,1},1}, antennas_tx::Int64, antennas_rx::Int64, radius, users_per_cell::Int64, channel_id::Int64, min_r = 0, max_r = 1)
  if min_r < 0 || min_r > 1 || max_r > 1 || max_r < 0 || min_r > max_r
    error("Invalid limits for the placement of a point in the cell")
  end

  ms_positions = HexagonalGrid.place_users(radius, bs_positions, users_per_cell, min_r, max_r)
  return get_channel_matrix(bs_positions, ms_positions, antennas_tx, antennas_rx, channel_id)
end

# ------------------------------------------------------------------------------
# Returns the parameters from a given channel id
function get_parameters(channel_id)
  if channel_id == 4
    parameters = (0.0, 1, 0)
  elseif channel_id == 2
    parameters = (3.8, 1.0, 32)
  else
    error("Channel id not supported")
  end
  return parameters
end

# ------------------------------------------------------------------------------
# Returns the attenuation at a given distance
function path_loss(distance, channel_id)

  path_loss_exponent, reference_distance, ref_dist_att = get_parameters(channel_id) 
  return 10^(-(ref_dist_att + 10 * path_loss_exponent * log10(distance / reference_distance)) / 10)
end

# ------------------------------------------------------------------------------
# Calculates the channel matrix for the case of having log-normal pathloss and
# shadowing, as well as flat fading.
function lognormal_channel_matrix(bs_positions, ms_positions, antennas_tx, antennas_rx, path_loss_exponent, reference_distance, ref_dist_att)
  number_of_bs = size(bs_positions, 1)
  number_of_ms = size(ms_positions, 1)
  
  if number_of_ms%number_of_bs != 0
    error("Non integer number of users per cell $(number_of_ms/number_of_bs)")
  end

  distances = calculate_distances(bs_positions, ms_positions)

  path_loss(d::Float64) = sqrt(10^(-(ref_dist_att + 10 * path_loss_exponent * log10(max(1, d) / reference_distance)) / 10) / 2)

  map!(path_loss, distances) 

  channel_matrix = kron(distances, ones(Complex{Float64}, antennas_rx, antennas_tx))

  rayleigh(x::Complex{Float64}) = x * (randn() + im * randn())

  map!(rayleigh, channel_matrix)

  return channel_matrix
end

# ------------------------------------------------------------------------------
# Calculates a matrix with the distances from all MS to all BS.
function calculate_distances(bs_positions::Array{Array{Float64,1},1}, ms_positions::Array{Array{Float64,1},1})
  number_of_bs = size(bs_positions, 1)
  number_of_ms = size(ms_positions, 1)
  distances = zeros(Float64, number_of_ms, number_of_bs)
  for ms in 1:number_of_ms, bs in 1:number_of_bs
    distances[ms, bs] = norm(bs_positions[bs] - ms_positions[ms])
  end
  return distances
end

function calculate_distances(bs_positions::Array{Float64,2}, ms_positions::Array{Float64,2})
  new_bs_positions = Array(Array{Float64, 1}, size(bs_positions, 1))
  new_ms_positions = Array(Array{Float64, 1}, size(ms_positions, 1))

  for ii in 1:size(bs_positions, 1)
    new_bs_positions[ii] = vec(bs_positions[ii, :])
  end

  for ii in 1:size(ms_positions, 1)
    new_ms_positions[ii] = vec(ms_positions[ii, :])
  end

  return calculate_distances(new_bs_positions, new_ms_positions)
end

# ------------------------------------------------------------------------------
# Generates the channel matrix when only flat-fading (Rayleigh) is present.
function flat_fading_channel_matrix(number_of_bs, tx_antennas, rx_antennas, users_per_cell = 1)
  number_of_bs = integer(number_of_bs)
  number_of_rows = number_of_bs * users_per_cell * rx_antennas
  number_of_cols = number_of_bs * tx_antennas
  return sqrt(1/2) * (randn(number_of_rows, number_of_cols) + im * randn(number_of_rows, number_of_cols))
end

end
