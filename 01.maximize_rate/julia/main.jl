include("PrecodingMatrix.jl")
include("PropagationModel.jl")
include("PowerAllocation.jl")
include("Interference.jl")
include("HexagonalGrid.jl")

# -----------------------------------------------------------------------------
# parameters
radius = 1300
antennas_tx = 3
antennas_rx = 2
p_max = 1
tiers = 1
users_per_cell = 1
channel_id = 2
location = "global"
shift = false # true: triangular; false: hexagonal

# -----------------------------------------------------------------------------
# get the parameters for the location selected of the users
location_range = (0, 1)
if location == "global"
  location_range = (0, 1)
elseif location == "center"
  location_range = (0, 0.25)
elseif location == "edge"
  location_range = (0.75, 1)
else
  error("Wrong location selected")
end
# get the BS positions
bs_positions = HexagonalGrid.get_grid(radius, tiers, shift)
number_of_bs = size(bs_positions, 1)
# auxiliary variables
tx_acc = kron(eye(number_of_bs), ones(antennas_tx)).'
rx_acc = kron(eye(number_of_bs), ones(antennas_rx)).'

# -----------------------------------------------------------------------------
# Control the loop
max_iter = 1000

for jj = -5:2.5:20
  rate_ms_bd = zeros(Float64, number_of_bs * max_iter)
  rate_ms_zf = zeros(Float64, number_of_bs * max_iter)
  rate_ms_ind = zeros(Float64, number_of_bs * max_iter)
  SNR = jj
  sigma_n = p_max * PropagationModel.path_loss(radius, channel_id)/10^(SNR/10)
  println("--------------")
  println("SNR $(SNR) dB")
  tic()
  for ii in 1:max_iter
    # get the channel
    H = PropagationModel.get_channel_matrix(bs_positions, antennas_tx, antennas_rx, radius, users_per_cell, channel_id, location_range...)

    # -----------------------------------------------------------------------------
    # global BD
    U_bd, S_bd, W_bd = PrecodingMatrix.bd_precoding(H, antennas_tx, antennas_rx)
    # p_tx_bd = PowerAllocation.equal_power_assignment(W_bd, antennas_tx, antennas_rx, p_max)
    p_tx_bd = PowerAllocation.scaled_waterfilling(W_bd, S_bd.*S_bd, number_of_bs, antennas_tx, p_max, sigma_n)

    stream_rate_bd = log2(1 + (p_tx_bd .* S_bd.*S_bd) / sigma_n)
    rate_ms_bd[(ii-1)*number_of_bs+1:ii*number_of_bs] = rx_acc * stream_rate_bd

    # -----------------------------------------------------------------------------
    # global ZF with normalization
    W_zf = PrecodingMatrix.zf_precoding(H)
    W_zf_norm = W_zf./sqrt(sum(abs2(W_zf), 1))
    S_zf = diag(real(H * W_zf_norm))
    p_tx_zf = PowerAllocation.scaled_waterfilling(W_zf_norm, S_zf.*S_zf, number_of_bs, antennas_tx, p_max, sigma_n)

    stream_rate_zf = log2(1 + (p_tx_zf .* S_zf .* S_zf) / sigma_n) 
    rate_ms_zf[(ii-1)*number_of_bs+1:ii*number_of_bs] = rx_acc * stream_rate_zf

    # -----------------------------------------------------------------------------
    # individual SVD
    U_ind, S_ind, W_ind = PrecodingMatrix.individual_svd_precoding(H, antennas_tx, antennas_rx)
    S_aux = Array(Array{Float64,1}, size(S_ind, 1))
    for kk in 1:size(S_ind, 1)
      S_aux[kk] = S_ind[kk] .* S_ind[kk]
    end
    p_tx_ind = PowerAllocation.wf_ra(S_aux, sigma_n, p_max)
    interf_ind = Interference.get_interference(H, U_ind, W_ind, p_tx_ind, antennas_tx, antennas_rx)
    rate_ms_ind[(ii-1)*number_of_bs+1:ii*number_of_bs] = Interference.rate_with_interference(S_ind, p_tx_ind, interf_ind, sigma_n, antennas_rx)

  end
  toc()

  print("Printing output files...")
  # -----------------------------------------------------------------------------
  # Printing the values
  bd_file_name = @sprintf("bd_%02dx%02d_s%+04d_t%02d_%03d_%s.txt", antennas_tx, antennas_rx, floor(10*SNR), tiers, 10*log10(p_max/1e-3), location)
  zf_file_name = @sprintf("zf_%02dx%02d_s%+04d_t%02d_%03d_%s.txt", antennas_tx, antennas_rx, floor(10*SNR), tiers, 10*log10(p_max/1e-3), location)
  ind_file_name = @sprintf("ind_%02dx%02d_s%+04d_t%02d_%03d_%s.txt", antennas_tx, antennas_rx, floor(10*SNR), tiers, 10*log10(p_max/1e-3), location)

  bd_file = open(bd_file_name, "w")
  zf_file = open(zf_file_name, "w")
  ind_file = open(ind_file_name, "w")

  print_bd(x) = @printf(bd_file, "%.10f\n", x)
  print_zf(x) = @printf(zf_file, "%.10f\n", x)
  print_ind(x) = @printf(ind_file, "%.10f\n", x)

  map(print_bd, rate_ms_bd)
  map(print_zf, rate_ms_zf)
  map(print_ind, rate_ms_ind)

  # -----------------------------------------------------------------------------
  # closing files
  close(bd_file)
  close(zf_file)
  close(ind_file)

  println("DONE")

  println("Mean rate per user:")
  println("  BD  = $(mean(rate_ms_bd))")
  println("  ZF  = $(mean(rate_ms_zf))")
  println("  IND = $(mean(rate_ms_ind))")
  println("Noise = $(sigma_n)")
end
return
