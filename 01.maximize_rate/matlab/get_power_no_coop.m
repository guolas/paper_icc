function p = get_power_no_coop(H,cell_number,antenna_tx,antenna_rx,sigma_n,R_min)

% keyboard

% to store the rate achieved per user
r_aux = zeros(cell_number,1);

% to store the power transmitted
p = -1;

% reorganize the channel matrix so that it is easier to work with
% it throughout the function
h = zeros(antenna_rx,antenna_tx,cell_number,cell_number);
for ii = 1:cell_number
    for jj = 1:cell_number
        h(:,:,ii,jj) = H((ii-1)*antenna_rx+1:ii*antenna_rx,(jj-1)*antenna_tx+1:jj*antenna_tx);
    end
end

% keyboard

% calculate the rate achieved in an interference limited scenario
for ii = 1:cell_number  
    % calculate the inteference from the other base stations
    interf = zeros(antenna_rx,antenna_rx);
    for jj = 1:cell_number
        if jj==ii
            continue
        end
        interf = interf + h(:,:,ii,jj)*h(:,:,ii,jj)';
    end
    % calculate the rate attained for this user
    r_aux(ii) = log2(real(det(eye(antenna_rx) + h(:,:,ii,ii)*h(:,:,ii,ii)'*inv(interf))));
    
end

if sum(r_aux<R_min)>0
    p = -1;
    return;
end
% get the index of the user with the lowest rate
aux_idx = find(r_aux==min(r_aux));

p_min = 0;
p_max = 1e30;
tol = 1e-5;
p_mean = (p_max+p_min)/2;

while abs(p_max-p_min)>tol
    interf = zeros(antenna_rx,antenna_rx);
    for ii = 1:cell_number
        if ii==aux_idx
            continue
        end
        interf = interf + h(:,:,aux_idx,ii)*p_mean*eye(antenna_tx)*h(:,:,aux_idx,ii)';
    end
    % calculate the rate attained for this user
    r_aux(aux_idx) = log2(real(det(eye(antenna_rx) + h(:,:,aux_idx,aux_idx)*p_mean*eye(antenna_tx)*h(:,:,aux_idx,aux_idx)'*inv(sigma_n*eye(antenna_rx)+interf))));
    r_diff = R_min-r_aux(aux_idx);
    if abs(r_diff)<tol && r_diff < 0
        % stop
        p = p_mean;
        return;
    else
        % not stop yet
        if r_diff < 0
            % the rate is too big yet, so power can be further reduced
            p_max = p_mean;
        else
            % the rate is too low yet, so power has to be increased
            p_min = p_mean;
        end
        p_mean = (p_max+p_min)/2;
    end
end

p = p_mean;
