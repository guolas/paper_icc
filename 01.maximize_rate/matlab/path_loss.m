function [PL] = path_loss(d,channel_id)

% channel parameters
switch channel_id
    case 1
        % log-normal with exponent 2
        min_d=1;
        n = 2;
        var_shadow = 1;
        shadow = 0; % no shadowing
        K = 0;
        
    case 2
        % log-normal with exponent 3.8
        min_d=1;
        n = 3.8;
        var_shadow = 1;
        shadow = 0; % no shadowing
        K = 32;
        
    case 3
        % log-normal with exponent 3.8
        min_d=1;
        n = 3.8;
        var_shadow = 1;
        shadow = 1; % shadowing
        K = 8;
        
    case 4
        % Rayleigh
        min_d=1;
        n=0;
        var_shadow = 0;
        shadow = 0;
        K = 0;
        
    case 5
        % log-normal with exponent 2.8
        min_d=1;
        n = 2.8;
        var_shadow = 1;
        shadow = 0; % no shadowing
        K = 0;
        
    case 6
        % log-normal with exponent 4.8
        min_d=1;
        n = 4.8;
        var_shadow = 1;
        shadow = 0; % no shadowing
        K = 0;

    otherwise
        PL = NaN;
        return
end

PL = -(K + 10*n*log10(abs(d)./min_d) + shadow*sqrt(var_shadow)*randn(size(d)));
