function [W,varargout] = get_precoding_matrix_bd(H_tot,cluster_number,cluster_size,antenna_tx,antenna_rx)

% to store the precoding matrix
W = zeros(cluster_size*antenna_tx,cluster_size*antenna_rx,cluster_number);
% to store the singular values
S = zeros(cluster_size*antenna_rx,1,cluster_number);
% to store the unitary matrix to use in the receiver
U = zeros(cluster_size*antenna_rx,cluster_size*antenna_rx,cluster_number);

[aux_null, bb] = regexp(version, '(\d+)\.(\d+)\..*', 'match', 'tokens');
clear aux_null;
aux_version = str2double(bb{1}{1}) + str2double(bb{1}{2});
new_version = aux_version >= 17;

% iterate through all the clusters
for jj = 1:cluster_number
    % get matrix for current cluster
    H_cluster = H_tot((jj-1)*cluster_size*antenna_rx+1:jj*cluster_size*antenna_rx,(jj-1)*cluster_size*antenna_tx+1:jj*cluster_size*antenna_tx);
    for kk = 1:cluster_size
        aux_range = (kk-1)*antenna_rx+1:kk*antenna_rx;
        % aggregate interference matrix
        if new_version
            H_tilde = removerows(H_cluster,'ind',aux_range);
        else
            H_tilde = removerows(H_cluster,aux_range);
        end
        
        % calculate the null-space basis
        [U_tilde,S_tilde,V_tilde] = svd(H_tilde);
        rank_H_tilde = rank(H_tilde);
        V_tilde_0 = V_tilde(:,rank_H_tilde + 1:end);
        
        % calculate the required precoding for maximizing the rate
        [U_aux,S_aux,V] = svd(H_cluster(aux_range,:)*V_tilde_0);
        V_1 = V(:,1:antenna_rx);
        % the precoding matrix for this user is
        W(:,aux_range,jj) = V_tilde_0*V_1;        
        % singular values for the independent streams of this user
        S(aux_range,:,jj) = diag(S_aux(1:antenna_rx,1:antenna_rx));
        % unitary matrix to use in the receiver to get the diagonal matrix
        U(aux_range,aux_range,jj) = U_aux;
    end
end

if nargout == 2
    varargout{1} = S;
elseif nargout == 3
    varargout{1} = S;
    varargout{2} = U;
end
