function cell_pos = get_scenario_hexagonal(cell_radius,tiers)
% GET_SCENARIO_HEXAGONAL  generate the scenario with cells placed in hexagonal
%                         tiers.
%    GET_SCENARIO_HEXAGONAL(CELL_RADIUS,TIERS) generates the scenario with cells
%    of CELL_RADIUS radius (three way corner radius) organized in TIERS tiers.
%    The returned value is the positions of the (3*TIERS*(TIERS+1)+1) cells as 
%    complex numbers (real and imaginary parts being the two orthogonal
%    coordinates.

cell_apothem = cell_radius*sqrt(3/4);

% position of base stations
cell_pos = 0;
cell_tier = 0;

for ii = 1:tiers
    aux_cell = cell_pos(cell_tier==ii-1);
    for jj = 1:length(aux_cell)
        aux_cell_pos = aux_cell(jj)+2*cell_apothem*exp(sqrt(-1)*(0:5)*pi/3);
        for kk = 1:length(aux_cell_pos)
            if sum(abs(cell_pos-aux_cell_pos(kk))<cell_radius) == 0
                cell_pos = [cell_pos;aux_cell_pos(kk)];
                cell_tier = [cell_tier;ii];
            end
        end
    end
end