function [P_unif] = get_uniform_power(P_max,W,cluster_number,cluster_size,antenna_tx,antenna_rx)
%GET_UNIFORM_POWER calculates the power to be transmitted if all the
%   symbols are transmitted using the same power and the maximum power
%   constraint is to be met.
%   
%   [P] = GET_UNIFORM_POWER(A,B,C,D,E,F) returns the power when the maximum
%   power is A, the precoding matrix is W, the number of clusters is C, the
%   cluster size D, and the antennas are ExF, at the transmitter and the
%   receiver, respectively.

% auxiliar variable used to accumulate the power from all the
% transmit antennas within a BS, giving the total power sent
% through each BS. It is just a matrix of the form:
%
% [ 1 1 1 0 0 0 0 0 0 0 0 0]
% [ 0 0 0 1 1 1 0 0 0 0 0 0]
% [ 0 0 0 0 0 0 1 1 1 0 0 0]
% [ 0 0 0 0 0 0 0 0 0 1 1 1]
%
% for a case with 4 BS and 3 tx antennas per BS.
PB_acc = kron(eye(cluster_size),ones(1,antenna_tx));

P_unif = zeros(cluster_size*antenna_rx,1,cluster_number);

for ii = 1:cluster_number
    
    % auxiliar variable used in the script to calculate power
    % transmitted per transmit antenna, from the power assigned
    % to each transmitted symbol. It's just the matrix W taking the
    % absolute value squared, elementwise.
    abs_W_2 = abs(W(:,:,ii)).^2;

    % power range, to be used with the bisection method to get the solution
    p_range = [0 2^5];

    % precission to stop the bisection search
    tol = 1e-3;

    p_mean = p_range(2);
    p_bs = PB_acc*abs_W_2*p_mean*ones(cluster_size*antenna_rx,1);
    while max(p_bs) < P_max
        p_range(2) = 2 * p_range(2);
        p_mean = p_range(2);
        p_bs = PB_acc*abs_W_2*p_mean*ones(cluster_size*antenna_rx,1);
    end

    p_mean = mean(p_range);
    p_bs = PB_acc*abs_W_2*p_mean*ones(cluster_size*antenna_rx,1);
    p_diff = p_bs-P_max;

    while sum(p_diff>0)~=0 || min(abs(p_diff))>tol
        if sum(p_diff>0) > 0
            % there are powers exceeding so lower half
            p_range = [p_range(1) p_mean];
        else
            % upper half
            p_range = [p_mean p_range(end)];
        end
        p_mean = mean(p_range);
        p_bs = PB_acc*abs_W_2*p_mean*ones(cluster_size*antenna_rx,1);
        p_diff = p_bs-P_max;
    end
    % so the transmitted power of the symbols is
    P_unif(:,:,ii) = p_mean*ones(cluster_size*antenna_rx,1);
end
P_unif = p_mean;