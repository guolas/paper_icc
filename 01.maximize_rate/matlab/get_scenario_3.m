function cell_pos = get_scenario_3(cell_radius)
% GET_SCENARIO_3 generates a scenario with three hexagonal cells.
%
%    GET_SCENARIO_3(CELL_RADIUS) generate the three cell with a (three way
%    corner) radius of CELL_RADIUS.
%    The value returned is the position of the cells as a vector of complex
%    numbers where real and imaginary parts represent the two orthogonal
%    coordinates (abscissa and ordinate).

cell_apothem = cell_radius*sqrt(3/4);

cell_pos = zeros(3,1);
cell_pos(1) = 0;
cell_pos(2) = 2*cell_apothem;
cell_pos(3) = 2*cell_apothem*exp(sqrt(-1)*pi/3);
