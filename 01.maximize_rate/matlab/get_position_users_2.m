% [2013-08-19 JJGARCIA]
% Users are placed in an hexagon, optionally selecting a range of the
% radius that the users can be located in, so that rings can be selected as
% location for the users.
% This version differs from the one made by Oscar in that this is using a
% cubic coordinates system, that makes it quite ... clean and easy to
% implement.

function ms_positions = get_position_users_2(bs_positions, radius, users_per_cell, min_r, max_r)
M = [cos(pi/2) -sin(pi/2); sin(pi/2) cos(pi/2)] *...
    [-cos(pi/6) cos(pi/6) 0.0; -sin(pi/6) -sin(pi/6) 1.0] * radius / sqrt(3);

number_of_bs = size(bs_positions, 1);
ms_positions = zeros(number_of_bs * users_per_cell, 2);
for ii = 1:number_of_bs
    for jj = 1:users_per_cell
        uvw = get_position_in_hexagon(min_r, max_r);
        ms_positions((ii-1)*users_per_cell + jj, :) = (M * uvw + bs_positions(ii, :).').';
    end
end
end

function uvw = get_position_in_hexagon(min_r, max_r)
uvw = zeros(3, 1);
idx1 = randi(3);
idx2 = mod((idx1 + 1), 3);
idx3 = mod((idx1 + 2), 3);
if idx2 == 0
    idx2 = 3;
end
if idx3 == 0
    idx3 = 3;
end
uvw(idx1) = sign(randn()) * sqrt(min_r^2 + rand * (max_r^2 - min_r^2));
uvw(idx2) = - uvw(idx1)*rand();
uvw(idx3) = - uvw(idx1) - uvw(idx2);
end