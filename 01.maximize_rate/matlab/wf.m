function [E,K] = wf(g,E_tot,alphas)
% WF Rate-Adaptive (RA) Water Filling algorithm.
%    E = WF(G,E_tot,alphas) calculates the Rate-Adaptive Waterfilling solution
%    for a given channel with response the vector G, including channel gains and
%    noise, when the maximum total energy to be used is the scalar E_tot,
%    and the priorities assigned to each subchannel are given by vector
%    alphas (sum(alphas)=1), giving the power to be assigned to each subchannel,
%    in the vector E.
%    [E,K] = WF(G,E_tot,alphas) also returns the water level K that should
%    be multiplied by the alphas to have the water level at each
%    subchannel.

% check if g is a column vector
[r,c] = size(g);

% if it is a column vector, change it into a row vector before the sorting
aux_flag = 0; % to mark that the vector is rotated for the sorting
if r>c
    g = g.';
    aux_flag = 1;
end

% sort the elements in g and get the indexes to unsort the result after
% processing
[g_sort,indexes] = sort(g);

% keyboard

% sort descending both channel response and indexes
g_sort = fliplr(g_sort);
indexes = fliplr(indexes);
alphas_sort = alphas(indexes);

% undo the rotation after sorting
if aux_flag
    g_sort = g_sort.';
    indexes = indexes.';
    g = g.';
end

% number of subchannels
N = max(r,c);

% inverse of channel response
g_sort_inv = 1./g_sort;

% sum of inverses of channel response
sum_g_inv = sum(g_sort_inv);

% sum of alphas
sum_alphas = sum(alphas_sort);

% initialize auxiliary constant
K = (E_tot+sum_g_inv)/sum_alphas;

% counter for active subchannels
N_star = N;

% main loop
while(N_star > 0)
    if (alphas_sort(N_star)*K - g_sort_inv(N_star)) <= 0
        sum_g_inv = sum_g_inv - g_sort_inv(N_star);
        sum_alphas = sum_alphas - alphas_sort(N_star);
        % deactivate subchannel
        N_star = N_star - 1;
        K = (E_tot+sum_g_inv)/sum_alphas;
    else
        break
    end
end

% compute WF energies and unsort subchannels
E_sort = zeros(size(g));
E = zeros(size(g));
E_sort(1:N_star) = alphas_sort(1:N_star)*K - g_sort_inv(1:N_star);
E(indexes) = E_sort;
