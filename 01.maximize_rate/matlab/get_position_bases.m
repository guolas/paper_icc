function [positionBases] = get_position_bases(size, radiusCell)
                                           

% FILE NAME:
%           get_position_bases.m
%
% INPUTS:
%
%           size: It is an integer that represents the
%           number of tires that form the scenario.
%           Here a Tiers is defined as the set of cell centers located at
%           the same distance to the center cell 
%           (Be aware of the difference between center cell and cell
%           center).
%
%
%           radiusCell: The radius of the cell 
%
% OUTPUTS:
%           positionBases: Matrix with the x y position of all base station refered to
%           the first one (origin at (0,0)). Size [nBases x 2]
%
% EXAMPLE: 
%           get_position_bases(3,1000) will return a matrix with the position of
%           all base stations inside the first 3 tiers with radius cell 1 km
%
% REMARKS:
%           Just working for the first 19 Tiers 
%
% AUTHOR:   Oscar Perez Navarro - Roberto Corvaja                                     
% EMAIL:   operez@tsc.uc3m.es                                        
% DATE OF CREATION:     December    18th,   2005
% LAST CHANGED:         March        2nd,   2011
%
% Other m-files required:   none
% Subfunctions:             none
% MAT-files required:       none
% See also: -


positionBases = [0 0];
D=sqrt(3)*radiusCell;         % distance of the centers from tier to tier

nTiers = size;
% We iterate for each Tier
for i=1:nTiers
  for j=1:6*i  %in tier i we have 6i cells
      x1=D*i*sin(2*pi*(j-1)/(6*i));
      y1=D*i*cos(2*pi*(j-1)/(6*i));
      positionBases=[positionBases; x1 y1];
  end
end
        
