function cluster = get_cluster_2(bs_center, bs_within, cluster_size, grouping)

% put together all the BS, including BS_CENTER
bss = [bs_center;bs_within];

% to mark what bs are already selected (0-> no sel;1-> sel)
bss_sel = zeros(size(bss));

% to store current cluster
cluster = zeros(cluster_size,1);
cluster(1) = 1;
baricentre = bss(cluster(1));
bss_sel(1) = 1;

% keyboard

for ii = 2:cluster_size
    id_no_sel = find(~bss_sel);
    distance = abs(baricentre-bss(~bss_sel));
    cluster(ii) = id_no_sel(find(distance==min(distance),1,'first'));
    bss_sel(cluster(ii)) = 1;
    % calculate the baricenter
    baricentre = ((ii-1)/(ii))*baricentre + (1/ii)*bss(cluster(ii));
end

% check the overall grouping of the cluster obtained
cur_val = max(abs(baricentre-bss(cluster)));
if cur_val > grouping
    % it does not meet the condition, so put the cluster to all -1
    cluster = -1*ones(size(cluster));
else
    % as BS_CENTER was put together with BS_WITHIN 1 should be substracted
    % so that the indexes are correct.
    cluster = cluster-1;
end

return
