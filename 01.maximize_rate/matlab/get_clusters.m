function clusters = get_clusters(cell_pos,cluster_size,approx_radius,grouping,random)

if cluster_size == 1
    clusters = 1:length(cell_pos(:));
    return
end

% all the positions of the BS in a column vector
cell_pos = cell_pos(:);

% variable to store the clusters
clusters = nan(cluster_size,ceil(numel(cell_pos)/cluster_size));

% to store the number of clusters
cluster_number = 0;

% variable to keep track of the BS already in a cluster it has two columns,
% the first to store the index itself and the second to store the status of
% that bs:
%   1 -> assigned
%   0 -> unassigned
%  -1 -> unable to assign properly
id_sel = zeros(numel(cell_pos),1);

baricentre = 0;

for ii = 1:numel(cell_pos)
    % check if there are enough unassigned elements to make a cluster
    if sum(~id_sel) < cluster_size
        % there are no more, so stop
        break;
    end
    if ii == 1
        if random
            %first iteration pick one cell randomly
            % CAREFUL with the rounding, do not use round, as it may round down
            % to 0.
            start_cell = ceil(numel(cell_pos)*rand);
        else
            start_cell = ceil(numel(cell_pos)/2);
        end
    else
        % select the cell that is not already assigned or has been
        % problematic and it is closest to the baricentre
        aux_no_sel = find(~id_sel);
        distance = abs(baricentre - cell_pos(~id_sel));
        start_cell = aux_no_sel(find(distance==min(distance),1,'first'));
    end
    id_sel(start_cell) = 1;
    id_no_sel = find(id_sel<1);

    % get the surrounding environment of the start_cell
    [cur_dist] = get_distance(cell_pos(start_cell),cell_pos(id_sel<1));
    
    % calculate the cells that are close to the center cell
    bs_within = find(cur_dist<2*approx_radius);
    
    % if there are not enough unassigned BS in the surroundings then
    % continue to the next BS
    if size(bs_within,1) < cluster_size-1
        id_sel(start_cell) = -1;
        continue;
    end
    
    cluster_aux_idx = get_cluster(cell_pos(start_cell),cell_pos(id_no_sel(bs_within)),cluster_size,grouping*approx_radius);
   
    % check if there were problems calculating the cluster
    if sum(cluster_aux_idx==-1)>0
        % there were problems with this, so mark the station as
        % "problematic"
        id_sel(start_cell) = -1;
    else
        cluster_number = cluster_number+1;
        % get the actual id for the BS
        cluster_aux_idx(1) = start_cell;
        cluster_aux_idx(2:end) = id_no_sel(bs_within(cluster_aux_idx(2:end)));
        % the cluster was generated properly, so store it
        clusters(:,cluster_number) = cluster_aux_idx;
        % mark the BS as assigned
        
        id_sel(cluster_aux_idx) = 1;
        
        % update the baricentre
        baricentre = ((cluster_number-1)/(cluster_number))*baricentre + (1/cluster_number)*mean(cell_pos(cluster_aux_idx));
        
        
        %%%%
        if cluster_number == 1
            clusters = clusters(:,1);
            return
        end
        %%%%
        
    end
end

% trim the return value so that it contains only the generated clusters
clusters = clusters(:,1:cluster_number);
