function [distanceBasesUsers] = get_distance_bases_users(positionBases,positionUsers)

% FILE NAME:
%           get_distance_bases_users.m
%
% INPUTS:
%           positionBases: Matrix with the position of all bases in the
%           system. The first column represent x components and second y
%           components. Size [nBases x 2]
%
%           positionUsers: Matrix with the position of all the users in
%           the sytem. The first column represent x components and second y
%           components. Size [nUsers x 2]
%
% OUTPUTS:
%           distanceBasesUsers: Matrix with distances between each Base Station
%           and each user. size [nBases x nUsers]
%
% EXAMPLE: 
%           get_distance_users_bases([1 1;2 2],[0 0]) returns the matrix [1.4142 2.8284]
%
% REMARKS:-
%
% AUTHOR:   Oscar Perez Navarro                                       
% EMAIL:   operez@tsc.uc3m.es                                        
% DATE OF CREATION:     January      9th,   2006
% LAST CHANGED:         January     31st,   2006
%
% Other m-files required:   none
% Subfunctions:             none
% MAT-files required:       none
% See also: -

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Just ((Xbases-Xusers).^2+(Ybases-Yusers).^2).^(1/2)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% reshape positionBases to tempBases with the size [nUsers x nBases*2(components)] in 
% order to get the term (Xbases-Xusers) and (Ybases-Yusers)
tempBases=reshape(positionBases',size(positionBases,1)*size(positionBases,2),1);
tempRowOnesA=ones(1,size(positionUsers,1));
tempBases=(tempBases*tempRowOnesA)';
% clear tempRowOnesA;

% reshape positionusers to tempUsers with the size [nUsers x nBases*2(components)] in 
% order to get the term (Xbases-Xusers) and (Ybases-Yusers)
tempUsers=reshape(positionUsers,size(positionUsers,1)*size(positionUsers,2),1);
tempRowOnesB=ones(1,size(positionBases,1));
tempUsers=(tempUsers*tempRowOnesB);
tempUsers=reshape(tempUsers,size(positionUsers,1),size(positionBases,1)*2);
% clear tempRowOnesB;

% We separate X and Y components for Bases and Users
tempBasesX=tempBases(:,1:2:size(tempBases,2)-1);
tempBasesY=tempBases(:,2:2:size(tempBases,2));
tempUsersX=tempUsers(:,1:2:size(tempUsers,2)-1);
tempUsersY=tempUsers(:,2:2:size(tempUsers,2));
% clear tempBases;
% clear tempUsers;

% We difference them
difUsersBasesX=tempUsersX-tempBasesX;
difUsersBasesY=tempUsersY-tempBasesY;
% clear tempUsersX;
% clear tempUsersY;

% Power of 2 of the differences
difUsersBasesXpot2=difUsersBasesX.*difUsersBasesX;
difUsersBasesYpot2=difUsersBasesY.*difUsersBasesY;
% clear difUsersBasesX;
% clear difUsersBasesY;

% We sum them as we wanted from the formula
sumPot2DifUsersBases=difUsersBasesXpot2+difUsersBasesYpot2;

% Result
distanceBasesUsers=sqrt(sumPot2DifUsersBases)';



