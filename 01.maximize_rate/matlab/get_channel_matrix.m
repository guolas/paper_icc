function [totalChannel_total,varargout] = get_channel_matrix(positionBases,AntennasTransmitter,AntennasReceiver,radius,user_per_cell,channel_id, r_min, r_max)

randn('state',sum(100*clock));
rand('twister',sum(100*clock));

positionUsers=get_position_users_2(positionBases, radius, user_per_cell, r_min, r_max);

distances=get_distance_bases_users(positionBases,positionUsers);

% correct distances to have a "minimum distance" different from 0
distances = max(distances,1e-3);

PathLoss = 10.^(path_loss(distances,channel_id)./10);

totalChannel_total = kron(PathLoss,ones(AntennasReceiver,AntennasTransmitter));
totalChannel_total = sqrt(totalChannel_total/2).*(randn(size(totalChannel_total))+sqrt(-1)*randn(size(totalChannel_total)));

% optional output parameter
if nargout>1
    varargout{1} = positionUsers;
end