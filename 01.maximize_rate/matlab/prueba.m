clear all
close all

% to use CVX
run('~/cvx/cvx_setup');

% Raileigh channel, with one user per cell.
% Comparison of

% parameters
cell_radius = 1300;
t = 3; % number of antennas at each transmitter
r = 2; % number of antennas at each receiver
p_max = 1; % in Watt
tiers = 1;
users_per_cell = 1;
channel_id = 2;
location = 'global'

% get the parameters for the location selected for the users
switch location
case 'global'
	location_range = [0, 1];
case 'center'
	location_range = [0, 0.25];
case 'edge'
	location_range = [0.75, 1];
otherwise
	disp('Wrong location selected');
	exit(-1);
end
% get BS positions
bs_positions = get_scenario_hexagonal(cell_radius, tiers);
M = size(bs_positions, 1);
% auxiliary variables
tx_acc = kron(eye(M),ones(1,t));
rx_acc = kron(eye(M), ones(1,r));

max_iter = 1000;

for jj = -5:2.5:20

    SNR = jj;
    sigma_n = p_max * 10^(path_loss(cell_radius, 2)/10) / 10^(SNR/10);
    
    disp('-----------------------');
    disp(['SNR ', num2str(SNR), ' dB']);

    rate_ms_bd_cvx = zeros(M * max_iter, 1);
    rate_ms_zf_cvx = zeros(M * max_iter, 1);
    
    tic
    for ii = 1:max_iter
        % Generate the channel matrix with Rayleigh fading
        H = get_channel_matrix([real(bs_positions) imag(bs_positions)], t, r, cell_radius, users_per_cell, channel_id, location_range(1), location_range(2));
        
        %% BD
        [W, S, U] = get_precoding_matrix_bd(H, 1, M, t, r);
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % USE CVX TO CALCULATE THE OPTIMUM SOLUTION
        S1=(abs(S).^2)./(sigma_n);
        abs_W_2 = abs(W).^2;
        cvx_begin quiet;
        variable p_aux(r*M,1);
        maximize det_rootn(diag(1+S1.*p_aux));
        subject to
        tx_acc*abs_W_2*p_aux<p_max;
        p_aux>=0;
        cvx_end;
        % power transmitted through the antennas at the BS that are
        % coordinated
        cvx_clear;
        
        stream_rate_bd_cvx = log2(1 + abs(S).^2 .* p_aux / sigma_n);
        rate_ms_bd_cvx((ii-1) * M + 1:ii * M) = rx_acc * stream_rate_bd_cvx;
        
        %% ZF
		% uniform power allocation
        W = pinv(H);

        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % USE CVX TO CALCULATE THE OPTIMUM SOLUTION
        abs_W_2 = abs(W).^2;
        cvx_begin quiet;
        variable p_aux(r*M,1);
        maximize det_rootn(diag(1+p_aux/sigma_n));
        subject to
        tx_acc*abs_W_2*p_aux<p_max;
        p_aux>=0;
        cvx_end;
        % power transmitted through the antennas at the BS that are
        % coordinated
        cvx_clear;

		stream_rate_zf_cvx = log2(1 + p_aux / sigma_n);
		rate_ms_zf_cvx((ii-1)*M+1:ii*M) = rx_acc * stream_rate_zf_cvx;
     end
    toc
    
    bd_cvx_file_name = sprintf('bd_cvx_%02dx%02d_s%+04d_t%02d_%03d_%s.txt', t, r, floor(10*SNR), tiers, 10*log10(p_max/1e-3), location);
    zf_cvx_file_name = sprintf('zf_cvx_%02dx%02d_s%+04d_t%02d_%03d_%s.txt', t, r, floor(10*SNR), tiers, 10*log10(p_max/1e-3), location);
    
    bd_cvx_file = fopen(bd_cvx_file_name, 'w');
    zf_cvx_file = fopen(zf_cvx_file_name, 'w');
    
    for ii = 1:M*max_iter
        fprintf(bd_cvx_file, '%.10f\n', rate_ms_bd_cvx(ii));
        fprintf(zf_cvx_file, '%.10f\n', rate_ms_zf_cvx(ii));
    end
    
    disp(['Mean BD cvx: ', sprintf('%.3f', mean(rate_ms_bd_cvx))]);
    disp(['Mean ZF cvx: ', sprintf('%.3f', mean(rate_ms_zf_cvx))]);
    fprintf('Noise = %.10e\n', sigma_n);
    
    fclose(bd_cvx_file);
    fclose(zf_cvx_file);
end
