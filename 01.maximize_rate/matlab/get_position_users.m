function [positionUsers] = get_position_users(positionBases,radiusCell,nUsers)

% FILE NAME:
%           get_position_users.m
%
% INPUTS:
%           positionBases: Matrix of size [nBases x 2] whit the position (x,y) of 
%           all bases in the system. Will be used in order to allocate users in each
%           cell.
%
%           radiusCell: The apotem of the hexagon. It is a scalar
%
%           nUsers: number of Users in each cell so the total amount of users in the system
%           is (nUsers*numCells)
%          
%
% OUTPUTS:
%           positionUsers: Matrix with the x y position of all users distributed 
%           around the system. Size [nUsers x 2]
%
%
% AUTHOR:   Oscar Perez Navarro                                       
% EMAIL:   operez@tsc.uc3m.es                                        
% DATE OF CREATION:     December    18th,   2005
% LAST CHANGED:         August      18th,   2013
%
% Other m-files required:   none
% Subfunctions:             none
% MAT-files required:       none
% See also: -

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% We generate a random position for each user inside the hexagon delimited by lines (y1,y2
% y3,y4,y5 and y6) and then we just need to displace them to each BS position so we have 
% users in the whole system.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% [20110518 JJGARCIA] changes made to speed up the function a bit.

rot_mat = [cos(pi/2) sin(pi/2); -sin(pi/2) cos(pi/2)];
positionUsers=zeros(size(positionBases,1)*nUsers,2);
D=sqrt(4/3)*radiusCell;
user_idx = 0;
for countBases=1:size(positionBases,1)
            countUsers=0;
            % we have to check we already got all the users we want in the cell
            while (countUsers<nUsers)
                % random position of a user inside a rectangle of base 2*D and high 2*R
                x=2*D*rand(1)-D;
                y=2*radiusCell*rand(1)-radiusCell;
                %lines that represent the hexagon
                y1=sqrt(3)*x+2*radiusCell;
                y2=radiusCell;
                y3=-sqrt(3)*x+2*radiusCell;
                y4=sqrt(3)*x-2*radiusCell;
                y5=-radiusCell;
                y6=-sqrt(3)*x-2*radiusCell;
                
                % we check the random position created in a square is inside the hexagon and we 
                % add the position of the cell we are interested in
                
                edge = 0;
                if edge
                    if(abs(sqrt((x).^2+(y).^2))<0.95*radiusCell)
                        continue;
                    end
                    switch size(positionBases,1)
                        case 2
                            switch countBases
                                case 1
                                    if(abs(atan2(x,y))>pi/6)
                                        continue;
                                    end
                                case 2
                                    if(abs(atan2(x,y))<5*pi/6)
                                        continue;
                                    end
                            end
                        case 3
                            switch countBases
                                case 1
                                    if(atan2(x,y)>pi/6 || atan2(x,y)<-pi/2)
                                        continue;
                                    end
                                case 2
                                    if(atan2(x,y)>-pi/2 && atan2(x,y)<5*pi/6)
                                        continue;
                                    end
                                case 3
                                    if(atan2(x,y)<pi/6 || atan2(x,y)>5*pi/6)
                                        continue;
                                    end
                            end
                        case 7
                            switch countBases
                                case 2
                                    if(atan2(x,y)>-pi/2 && atan2(x,y)<pi/2)
                                        continue;
                                    end
                                case 3
                                    if(atan2(x,y)>-5*pi/6 && atan2(x,y)<pi/6)
                                        continue;
                                    end
                                case 4
                                    if(atan2(x,y)<-pi/6 || atan2(x,y)>5*pi/6)
                                        continue;
                                    end
                                case 5
                                    if(atan2(x,y)<-pi/2 || atan2(x,y)>pi/2)
                                        continue;
                                    end
                                case 6
                                    if(atan2(x,y)<-5*pi/6 || atan2(x,y)>pi/6)
                                        continue;
                                    end
                                case 7
                                    if(atan2(x,y)>-pi/6 && atan2(x,y)<5*pi/6)
                                        continue;
                                    end
                            end
                    end
                end
                
                if((x>=-D)&&(x<-D/2))
                    if((y<=y1)&&(y>=y6))
                        user_idx = user_idx +1;
%                         positionUsers=[positionUsers; [x y]+positionBases(countBases,:)];
                        positionUsers(user_idx,:) = [x y] * rot_mat +positionBases(countBases,:);
                        countUsers=countUsers+1;
                    end
                elseif((x>=-D/2)&&(x<D/2))
                    if((y<=y2)&&(y>=y5))
                        user_idx = user_idx +1;
%                         positionUsers=[positionUsers; [x y]+positionBases(countBases,:)];
                        positionUsers(user_idx,:) = [x y] * rot_mat +positionBases(countBases,:);
                        countUsers=countUsers+1;
                    end
                elseif((x>=D/2)&&(x<D))
                    if((y<=y3)&&(y>=y4))
                        user_idx = user_idx +1;
%                         positionUsers=[positionUsers; [x y]+positionBases(countBases,:)];
                        positionUsers(user_idx,:) = [x y] * rot_mat +positionBases(countBases,:);
                        countUsers=countUsers+1;
                    end
                end
            end
%             clear countUsers;
end
 
 
