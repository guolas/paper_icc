module ProcessResults
function get_cdf(strategy, snr, tiers)
  snr_str = @sprintf("%+04d", 10 * snr)
  input_file = "/Users/jjgarcia/temp/data/individual/$(strategy)_03x02_s$(snr_str)_t$(tiers)_030_global.txt"
  output_file = "/Users/jjgarcia/temp/data/cdf/cdf_$(strategy)_s$(snr_str)_t$(tiers).txt"
  number_of_bins = 250
  data = readcsv(input_file)
  bins, values = hist(data, number_of_bins)
  values_normalized = values ./ sum(values)
  cdf_data = cumsum(values_normalized)
  ofd = open(output_file, "w")
  printf(x, y) = @printf(ofd, "%.10f, %10f\n", x, y)
  map(printf, bins[2:end], cdf_data)
  close(ofd)
end

function get_mean_rate(strategy, tiers, snr_range, position, antennas_tx, antennas_rx)
  path = "/Users/jjgarcia/temp/data/rate_max"
  tiers_str = @sprintf("%02d", tiers)
  tx_str = @sprintf("%02d", antennas_tx)
  rx_str = @sprintf("%02d", antennas_rx)
  output_file = "$(path)/rate_vs_snr/mean_rate_$(strategy)_t$(tiers_str)_p030_$(tx_str)x$(rx_str)_$(position).txt"
  ofd = open(output_file, "w")
  for ii in 1:snr_range.len
    snr = @sprintf("%+04d", 10*snr_range[ii])
    input_file = "$(path)/individual/$(strategy)_$(tx_str)x$(rx_str)_s$(snr)_t$(tiers_str)_030_$(position).txt"
    data = readcsv(input_file)
    @printf(ofd, "%.10f, %10f\n", snr_range[ii], mean(data))
  end
  close(ofd)
end


end
