tiers = 1 
antennas_tx = 3
antennas_rx = 3
location = "global"
path = "~/temp/data/rate_max/rate_vs_snr"

set terminal x11
set autoscale
set grid
unset log
unset label
set xtic auto
set ytic auto
set title sprintf("Mean rate for a %d tier(s) network, users over the entire cell, %02dx%02d", tiers, antennas_tx, antennas_rx)
set xlabel "SNR (dB)"
set ylabel "Mean rate per MS (b/s/Hz)"
set xrange [-5:20]
set yrange [0:25]
set key top left

plot sprintf("%s/mean_rate_bd_t%02d_p030_%02dx%02d_%s.txt", path, tiers, antennas_tx, antennas_rx, location) title "Global BD (sWF)" with linespoints,\
     sprintf("%s/mean_rate_zf_t%02d_p030_%02dx%02d_%s.txt", path, tiers, antennas_tx, antennas_rx, location) title "Global ZF (sWF)" with linespoints,\
     sprintf("%s/mean_rate_ind_t%02d_p030_%02dx%02d_%s.txt", path, tiers, antennas_tx, antennas_rx, location) title "Individual SVD" with linespoints
replot

set terminal postscript eps
set output sprintf("~/mean_rate_vs_snr_t%02d_p030_%02dx%02d_%s.eps", tiers, antennas_tx, antennas_rx, location)
replot
set terminal x11
